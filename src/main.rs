#![recursion_limit = "1024"]

extern crate clap;
#[macro_use]
extern crate error_chain;

use clap::{App, Arg};
use std::io::{stdin, stdout, Write};
use std::process::Command;

mod errors {
    // use std::process::ExitStatus;

    error_chain! {
        // errors {
        //     NonZeroExitCode(stderr: String, status: ExitStatus)
        // }
    }
}

use errors::*;

const VERSION: &'static str = "0.2";

fn read_input() -> Result<String> {
    let mut buf = String::new();
    stdin()
        .read_line(&mut buf)
        .chain_err(|| "failed to read input")?;
    Ok(buf)
}

fn run_command(cmd: &str, default_args: &[&str]) -> Result<String> {
    let output = Command::new(cmd)
        .args(default_args)
        .output()
        .chain_err(|| "failed to get output")?;
    // if !output.status.success() {
    //     bail!("command exited with non-zero exit code");
    // }
    let stdout =
        String::from_utf8(output.stdout).chain_err(|| "failed to convert stdout to string")?;
    let stderr =
        String::from_utf8(output.stderr).chain_err(|| "failed to convert stderr to string")?;
    Ok(stdout + &stderr)
}

fn update_cmdline(command: &str, args: &[&str]) -> String {
    format!(
        "{}{}{}",
        command,
        if args.is_empty() { "" } else { " " },
        args.join(" ")
    )
}

fn run() -> Result<()> {
    let matches = App::new("sushelf")
        .version(VERSION)
        .author("Oleksii Filonenko <brightone@protonmail.com>")
        .arg(Arg::with_name("command").required(true))
        .arg(
            Arg::with_name("default_args")
                .multiple(true)
                .allow_hyphen_values(true),
        )
        .get_matches();

    let command = matches.value_of("command").unwrap();
    let mut default_args: Vec<&str> = matches
        .values_of("default_args")
        .map(|a| a.collect())
        .unwrap_or(Vec::new());
    let mut cmdline = update_cmdline(command, &default_args);
    let mut input: String;

    let result = loop {
        print!("{}> ", cmdline);
        stdout().flush().chain_err(|| "failed to flush stdout")?;
        input = read_input()?;
        if input.is_empty() {
            break;
        } else {
            if input.starts_with("cd ..") {
                default_args.pop();
                cmdline = update_cmdline(command, &default_args);
            } else if input.starts_with("cd ") {
                // let mut path: Vec<&str> = input.as_str().split_whitespace().skip(1).collect();
                // default_args.append(&mut path);
                // cmdline = update_cmdline(command, &default_args);
            } else {
                let mut args: Vec<&str> = input.split_whitespace().collect();
                args.extend_from_slice(&default_args);
                let output = run_command(command, &args).chain_err(|| "failed to run command")?;
                println!("{}", output);
            }
        }
    };
    Ok(result)
}

quick_main!(run);
