build:
	cargo build

release:
	cargo build --release

run:
    cargo run

test: build
	cargo test

install: release
	sudo install target/release/sushelf /usr/local/bin/sushelf
